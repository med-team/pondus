��    C      4  Y   L      �     �     �  #   �  �   �     ~  
   �     �     �  
   �     �     �     �     �            *   +     V     c     {     �     �     �     �  
   �  	   �     �  3     $   <     a     f     r     ~     �     �     �     �  	   �     �     �     �     	     	     '	  .   .	     ]	  .   x	     �	  -   �	     �	     
  8   
     Q
     X
     l
     x
  #   �
     �
     �
     �
     �
     �
     �
     �
     �
  
   �
     �
  �  �
  
   �  
   �  "   �  �   �     �     �     �     �     �     �            +   "     N     S  %   e     �     �     �     �     �     �               #     /  2   M  *   �     �     �     �     �     �     �     �          "  
   0     ;     J     d     r       *   �  $   �  2   �  #   
  8   .  *   g     �  4   �     �     �     �     �          $     '  
   *     5     8     ;     ?  	   A     K     T         :   @       
      (   !   2   7         <              9            3   -   1      A   0                 /                 )          5          ,   +           '      "          .      *   %              C   8      4          	   6   ;         &   >                 B          ?                        #       =   $    Add Dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Body Mass Index CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Do you really want to delete this dataset? Edit Dataset Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last Month Last Year Matplotlib not available! Not owning the file lock. Backing up the data to %s Please make sure pygtk is installed. Plot Plot Weight Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Smooth The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! Use Calendar in Add Dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-26 17:41+0000
Last-Translator: eike <eike@ephys.de>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Dodaj dane Cały czas Wystąpił błąd podczas importu. Wyglada na to ze inna kopia programu próbuje edytować ten sam plik danych. Czy na pewno chcesz kontynuować i utracić wszystkie zmiany z innej kopii programu? Indeks Masy Ciała Eksport do pliku CSV Plik CSV do przeczytania: Plik CSV do zapisania: Import z pliku CSV Nie można odnaleźć %s Własny Dane do eksportu: Plik danych jest zablokowany, kontynuować? Data Data (YYYY-MM-DD) Czy na pewno chcesz usunąć te dane? Edytuj dane Błąd: niepoprawny plik Błąd: Nieprawidłowy format Pomyślny eksport Importuj dane do: Błąd importu Pomyślny import Ostatni miesiąc Ostatni rok Matplotlib jest niedostępny! Brak pliku blokady. Zapisywanie kopii danych do %s Upewnij się że pygtk jest zainstalowane. Wykres Wykres wagi Preferencje System jednostek miar: Wyjście Czytanie pliku %s Pamiętaj rozmiar okna Usunąć dane? Zapisz wykres Typ pliku: Zapis do pliku Zapisywanie wykresu do %s Zakres czasu: Wybierz plik Gładki Wprowadzona data ma nieprawidłowy format! Eksport zakończył się pomyślnie. Podana ścieżka nie prowadzi do poprawnego pliku! Import zakończył się pomyślnie. Data początku musi być wcześniejsza niż data końca! Użyj kalendarza podczas dodawania wpisów Wzrost użytkownika: Użycie standardowego pliku ~/.pondus/user_data.xml. Waga Pomiary wagi Planner wagi Planner wagi To jest katalog a nie plik! cm ft anglosaski in kg lbs m metryczny waga.csv wykres_wagi.png 