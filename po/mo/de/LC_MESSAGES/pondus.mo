��    \      �     �      �     �     �     �  #   �  �        �     �     �  
   �     �     	  
   	     %	     7	  
   >	     I	     Y	     t	     y	     �	  *   �	     �	     �	     �	     
     
     (
     <
     N
     ^
     t
     �
  
   �
  	   �
     �
     �
     �
  3   �
       $        -     2     >     O     [     r     w     �     �     �  	   �     �     �     �     �     �       	   %     /  
   6  .   A     p  .   �     �  -   �  <     J   @     �     �     �     �  S   �     !  8   .     g     m     t     �     �  #   �     �     �     �     �     �     �     �     �  6   �  
         +  �  ;     �     �       '   !  �   I     �            
        *     @  
   O     Z  	   x     �     �     �     �     �     �  0   �     -     ?     _     x     �     �     �     �     �     �               !     .     K     X  5   ^     �  5   �     �     �     �               5     =     S     l     �     �     �     �      �     �        "        4  
   B     M  7   Z     �  $   �     �  .   �  :     S   Y     �     �     �  '   �  m        �  =   �     �     �     �     �       5        P     S     V     h     k     n     r     t  B   }     �     �     =       :       J   R   &              +                K       Y   >             @         [   !   '          5   Q       .       F   3          )      A   C       ,      ;       8   /   "   <   ?   %   4                      #   B          7                      I       X              $      L          9       N   O   Z   H   2                U       *   S   T   P   D   -   
   (                   6   G      E       V           1   M       \   	   W             0        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Append Notes to Datasets Body Mass Index Bodyfat CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data Left: Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Muscle None Not owning the file lock. Backing up the data to %s Note Please make sure pygtk is installed. Plot Plot Weight Plot weight data Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Right: Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Select date range of plot Show Plan Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Track Bodyfat Track Muscle Track Water Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Water Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-26 18:43+0000
Last-Translator: eike <eike@ephys.de>
Language-Team: German (http://www.transifex.net/projects/p/pondus/team/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1)
 Füge Datensatz hinzu Füge Datensatz hinzu Alle Zeiten Beim Import ist ein Fehler aufgetreten. Eine andere Instanz von Pondus scheint die selbe Datei zu bearbeiten. Möchtest du wirklich fortfahren und alle Änderungen der anderen Instanz verlieren? Notizen an Datensatz anhängen Body Mass Index Körperfett CSV Export Zu lesende CSV Datei: CSV Zieldatei: CSV Import Konnte Datei %s nicht finden. Angepasst Daten links: Zu exportierende Daten: Datei gesperrt, fortfahren? Datum Datum (JJJJ-MM-TT) Lösche ausgewählten Datensatz Möchtest du diesen Datensatz wirklich löschen? Ändere Datensatz Ändere ausgewählten Datensatz Aktiviere Gewichtsplaner Enddatum Fehler: Keine Datei Fehler: Falsches Format Export erfolgreich Importiere Daten nach: Import nicht erfolgreich Import erfolgreich Letzte 6 Monate Letzter Monat Letztes Jahr Matplotlib nicht verfügbar! Muskelanteil Keine Datei ist gesperrt. Die Daten werden in %s gesichert. Notiz Bitte stellen Sie sicher, dass PyGTK installiert ist. Diagramm Grafischer Gewichtsverlauf Grafischer Gewichtsverlauf Optionen Bevorzugtes Einheitensystem: Beenden Datei %s wird gelesen Speichere Fenstergröße Datensatz löschen? Rechts: Diagramm speichern Speichere Datei im Format: Speichere in Datei Diagramm wird als %s gespeichert Wähle Datumsbereich: Wähle Datei aus Wähle Datumsbereich des Diagramms Plan anzeigen Geglättet Anfangsdatum Die eingegebenen Daten haben nicht das richtige Format! Der Export war erfolgreich. Der angegebene Pfad ist keine Datei! Der Import war erfolgreich. Das Anfangsdatum muss vor dem Enddatum liegen! Der Gewichtsplaner kann im Optionsdialog aktiviert werden. Um den BMI darzustellen, muss die Körpergröße im Optionsdialog angegeben werden. Körperfett speichern Muskelanteil speichern Wasseranteil speichern Benutze Kalender im Hinzufügen-Fenster Benutze ein Kalenderwidget anstatt des Texteingabefeldes zum Eingeben von Daten im Hinzufügen-/Änderndialog Körpergröße: Benutze stattdessen die Standarddatei ~/.pondus/user_data.xml Wasseranteil Gewicht Gewichtsmessungen Gewichtsplaner Gewichtsplaner Sie haben ein Verzeichnis und keine Datei übergeben! cm ft angloamerikanisch in kg lbs m metrisch python-matplotlib ist nicht installiert, Plotfunktion deaktiviert! gewicht.csv gewichtsdiagramm.png 