��    \      �     �      �     �     �     �  #   �  �        �     �     �  
   �     �     	  
   	     %	     7	  
   >	     I	     Y	     t	     y	     �	  *   �	     �	     �	     �	     
     
     (
     <
     N
     ^
     t
     �
  
   �
  	   �
     �
     �
     �
  3   �
       $        -     2     >     O     [     r     w     �     �     �  	   �     �     �     �     �     �       	   %     /  
   6  .   A     p  .   �     �  -   �  <     J   @     �     �     �     �  S   �     !  8   .     g     m     t     �     �  #   �     �     �     �     �     �     �     �     �  6   �  
         +  �  ;     �     �     �  ,   �  �         �     �               &     =     X     h     w     }     �  +   �     �     �     �  4        9      O  *   p     �     �     �     �     �          *     E  	   N     X     d     ~     �  0   �     �  +   �     �     �       	   1     ;     X     ^     s     �     �     �     �     �     �       
     $   *     O     ^     j  $   �  /   �  /   �  .   	  >   8  H   w  b   �     #  
   8  	   C  '   M  o   u     �  E   �     A  
   D     O     g     y  !   �     �     �     �     �     �     �     �     �  K   �  
   #     .     =       :       J   R   &              +                K       Y   >             @         [   !   '          5   Q       .       F   3          )      A   C       ,      ;       8   /   "   <   ?   %   4                      #   B          7                      I       X              $      L          9       N   O   Z   H   2                U       *   S   T   P   D   -   
   (                   6   G      E       V           1   M       \   	   W             0        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Append Notes to Datasets Body Mass Index Bodyfat CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data Left: Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Muscle None Not owning the file lock. Backing up the data to %s Note Please make sure pygtk is installed. Plot Plot Weight Plot weight data Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Right: Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Select date range of plot Show Plan Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Track Bodyfat Track Muscle Track Water Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Water Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-30 11:21+0000
Last-Translator: kara <muhammet.k@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=1; plural=0
 Veri Kümesi Ekle Veri kümesi ekle Tüm Zamanlar İçe aktarım sırasında bir hata oluştu. pondus'un başka bir örneği aynı veri dosyasını düzenliyor gibi görünüyor. Gerçekten devam etmek istiyor musunuz? (Diğer örnekteki tüm değişiklikleri kaybedeceksiniz.) Veri Kümelerine Not Ekle Vücut Kitle İndeksi Vücut Yağı CSV Dışa Aktar Okunacak CSV Dosyası: Kaydedilecek CSV Dosyası: CSV İçe Aktar %s bulunamadı Özel Kalan Veri: Dışa aktarılacak veri: Veri dosyası kilitlendi, devam edilsin mi? Tarih Tarih (YYYY-AA-GG) Seçilen veri kümesini sil Gerçekten bu veri kümesini silmek istiyor musunuz? Veri Kümesi Düzenle Seçilen veri kümesini düzenle Ağırlık Planlayıcısını Etkinleştir Bitiş tarihi Hata: Geçerli bir dosya değil Hata: Yanlış Biçim Dışa aktarım başarılı Veriyi içe aktar: İçe aktarım başarısız İçe aktarım başarılı Son 6 Ay Geçen Ay Geçen Yıl Matplotlib mevcut değil! Kas Hiçbiri Dosya kilidi yok. Veriler %s içine yedekleniyor Not Lütfen pygtk kurulu olduğundan emin olun. Grafik çiz Ağırlık Grafiği Çiz Ağırlık verisi grafiği çiz Tercihler Tercih Edilen Birim Sistemi: Kapat %s dosyası okunuyor Pencere Boyutunu Hatırla Veri Kaldırılsın mı? Sağ: Grafiği Kaydet Farklı Kaydet Dosya Türü: Dosyaya Kaydet Grafik %s içine kaydediliyor Tarih Aralığı Seçin: Dosya Seç Grafiğin tarih aralığını seçin Planı Göster Pürüzsüz Başlangıç ​​tarihi Girilen veri doğru biçimde değil! Dışa aktarım başarıyla gerçekleştirildi. Verilen yol geçerli bir dosyayı göstermiyor! İçe aktarım başarıyla gerçekleştirildi. Başlangıç ​​tarihi bitiş tarihinden önce olmalıdır! Ağırlık planlayıcısı, tercihler iletişim kutusundan açılabilir. VKİ grafiğinizin çizilebilmesi için, tercihler ​​iletişim kutusuna boyunuzu girmelisiniz. Vücut Yağı Takibi Kas Takibi Su Takibi Ekle İletişim Kutusunda Takvim Kullan Ekle/düzenle iletişim kutusunda tarihleri girmek için metin girişi yerine bir takvim uygulamacığı kullan Kullanıcının Boyu: Onun yerine ~/.pondus/User_data.xml standart dosyası kullanılıyor. Su Ağırlık Ağırlık Ölçümleri Ağırlık Planı Ağırlık Planlayıcısı Bir dosya değil, dizin girdiniz! cm ft ingiliz in kg lbs m metrik python-matplotlib kurulu değil. Grafik çizimi devre dışı bırakıldı! weight.csv weight_plot.png 