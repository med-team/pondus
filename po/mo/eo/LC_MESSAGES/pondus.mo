��    =        S   �      8     9     E     Q  �   Z     �  
   �  
   	          &     -     =     X     ]     o  *   �     �     �     �     �     �                2  
   @  	   K     U  $   o     �     �     �     �     �     �     �     �  	   �     	          )     <     H     b  
   i     t  8   �     �     �     �     �  #   �     	     	     	     #	     &	     )	     -	     /	  
   6	     A	  �  Q	     �
     �
     �
  �   �
     �     �     �     �     �     �  (   �               (  ,   C     p     �     �     �     �     �     �            
        *  %   J  
   p     {     �     �     �     �     �     �               3     M     e  -   u     �     �     �  9   �          	  
        "     *     G     J     M     U     X     [     _     a  
   g     r                   5       $   -         1      .       4   (                ,       8          )                     *   '       %      "         =      9                 /       +       
       6      :   0      <       2   #      &       7   ;   !                   3                       	            Add Dataset Add dataset All Time Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Body Mass Index CSV Export CSV Import Could not find %s Custom Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Last 6 Months Last Month Last Year Matplotlib not available! Please make sure pygtk is installed. Plot Plot Weight Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Save Plot Save as File Type: Save to File Select Date Range: Select File Select date range of plot Smooth Start date User Height: Using the standard file ~/.pondus/user_data.xml instead. Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-26 17:41+0000
Last-Translator: eike <eike@ephys.de>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eo
Plural-Forms: nplurals=2; plural=(n != 1)
 Aldoni Datumaron Aldoni Datumaron Ĉiam Ŝajnas ke alia apero de pondus redaktas la saman datumdosieron. Ĉu vi vere deziras daŭrigi kaj perdi ĉiujn ŝanĝojn de la alia apero ? Korpomasa indico CSV Eksportado CSV Importado Ne eblis trovi %s Propra Datumoj por eksporti: Datumdosiero ŝlosita, ĉu ni daŭrigu ? Dato Dato (JJJJ-MM-TT) Forigi elektitan datumaron Ĉu vi vere volas forigi ĉi tiun datumaron? Redakti  Datumaron Redakti elektitan datumaron Ebligi Pezcelon Dato de fino Eraro: Ne valida Dosiero Eraro: Netaŭga Formato Eksportado suksesis Lastaj 6 Monatoj Lasta Monato Lasta Jaro Matplotlib ne estas disponebla. Bonvolu certiĝi ke pygtk instaliĝis Grafikaĵo Grafika Reprezento de Pezo Agordoj preferata Unua Sistemo: Forlasi Legante dosieron %s Memoru fenestran grandecon Ĉu forigi Datumojn? Konservi grafikaĵon Konservi kiel dosiera tipo: Konservi al la dosiero... Elektu datan intervalon Elekti dosieron Elektu datan intervalon de grafika reprezento Glata Dato de komenco Uzanta Grandeco: Uzi aprioran dosieron  ~/.pondus/user_data.xml anstataŭ. Pezo Pezaj Mezuroj Peza Plano Pezcelo Estas dosierujo, ne dosiero! cm ft imperia in kg lbs m metra weight.csv weight_plot.png 