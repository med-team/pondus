��    P      �  k         �     �     �     �  #   �  �        �  
   �     �     �  
   �     �                    8     =     O  *   g     �     �     �     �     �     �      	     	     "	     8	     J	  
   X	  	   c	     m	  3   �	  $   �	     �	     �	     �	     
     
     %
     *
     :
     O
  	   \
     f
     y
     �
     �
     �
     �
     �
  
   �
  .   �
       .   -     \  -   w  <   �  J   �     -  S   H     �  8   �     �     �     �     	  #        <     ?     B     K     N     Q     U     W  6   ^  
   �     �  �  �     6     P     j  *   y  �   �     =     V     f     �     �     �  	   �     �  (   �            %   $  2   J     }  %   �     �     �     �       !   (     J     ]     u     �     �     �     �  R   �  ;   )     e     n     �     �     �     �     �     �     �               %     7     R     n     �     �     �  :   �  (   �  +     &   F  :   m  :   �  K   �  -   /  k   ]     �  3   �               %     2  $   G     l     o     t     }     �     �     �     �  <   �     �     �     ?      @   C       "   L   A              /   K           E   %                   F      7   H   .       N               	      8   >   5      4   M       6   !                  ,                  $   '   #   :   ;      )   *   2         P               0                 &       O      I           -       B      1                 <          D       (      =   9   
         +   J   3   G        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Body Mass Index CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Not owning the file lock. Backing up the data to %s Please make sure pygtk is installed. Plot Plot Weight Plot weight data Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Select date range of plot Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-26 17:41+0000
Last-Translator: eike <eike@ephys.de>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1)
 Añadir conjunto de datos Añadir conjunto de datos Todo el tiempo Un error occurrió durante la importación Otra instancia de pondus parece estar editando el mismo archivo de datos. ¿Realmente quieres continuar y perder todos los cambios de la otra instancia? Índice de Masa Corporal Exportar a CSV  Archivo CSV para leer desde:  Archivo CSV para guardar en: Importar CSV  No se pudo encontrar %s Costumbre Datos a exportar Archivo de datos bloqueado, ¿continuar? Fecha Fecha (AAAA-MM-DD) Borrar conjunto de datos seleccionado ¿Realmente quieres borrar este conjunto de datos? Editar conjunto de datos Editar conjunto de datos seleccionado Usar Planificador de Peso: Fecha de finalización Error: El Archivo no es válido Error: formato incorrecto Exportación realizada con éxito Importar datos de: Importación sin éxito Importación con éxito Últimos 6 Meses Último Mes Último Año Matplotlib no está disponible No tiene permisos sobre el Archivo. Guardando copia de seguridad de los datos a %s Por favor, comprueba que la librería pygtk está instalada Gráfico Gráfico del peso Gráfico del peso Preferencias Sistema de Medida Seleccionado Salir Leyendo archivo %s Recordar el tamaño de ventana ¿Borrar datos? Guardar Gráfico Guardar como: Guardar a Archivo Guardando el gráfico a %s Seleccionar Rango de Fechas Seleccionar Archivo Seleccionar Rango de Fechas Suave Fecha de comienzo ¡Los datos introducidos no están en el formato correcto! La exportación fue realizada con éxito La ruta dada no indica a un fichero válido La importación se realizó con éxito La fecha de inicio tiene que ser anterior a la fecha final El planificador de peso puede ser activado en Preferencias Para hacer el gráfico de tu IMC debes introducir tu altura en preferencias Usar Calendario en el Cuadro de añadir datos Usa un widget de calentario en vez de una entrada de texto para ñadir fechas en el diálogo añadir/editar Altura del Usuario Usando el fichero estándar ~/.pondus/user_data.xml Peso Medidas de peso Plan de peso Planificador de Peso Esto es un directorio, no un archivo cm pies imperial pulgadas kg lbs m métrico python-Matplotlib no está instalado, gráfico deshabilitado peso.csv grafico_peso.png 