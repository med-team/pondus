��    \      �     �      �     �     �     �  #   �  �        �     �     �  
   �     �     	  
   	     %	     7	  
   >	     I	     Y	     t	     y	     �	  *   �	     �	     �	     �	     
     
     (
     <
     N
     ^
     t
     �
  
   �
  	   �
     �
     �
     �
  3   �
       $        -     2     >     O     [     r     w     �     �     �  	   �     �     �     �     �     �       	   %     /  
   6  .   A     p  .   �     �  -   �  <     J   @     �     �     �     �  S   �     !  8   .     g     m     t     �     �  #   �     �     �     �     �     �     �     �     �  6   �  
         +  �  ;     �            !      �   B  $   �       
     
         +     D  
   Y     d     x     �     �     �     �     �     �  %   �                4  	   F     P     a     w     �     �     �     �     �     �     �            +        E  "   K     n     v     �     �     �     �     �     �     �     �               $     3     J  	   \  %   f  	   �  	   �  
   �  +   �     �  4   �       %   2  ;   X  M   �     �     �        #     X   1     �  8   �     �     �     �     �  	   �          $     '     +     4     8     ;     @     B  <   K     �     �     =       :       J   R   &              +                K       Y   >             @         [   !   '          5   Q       .       F   3          )      A   C       ,      ;       8   /   "   <   ?   %   4                      #   B          7                      I       X              $      L          9       N   O   Z   H   2                U       *   S   T   P   D   -   
   (                   6   G      E       V           1   M       \   	   W             0        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Append Notes to Datasets Body Mass Index Bodyfat CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data Left: Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Muscle None Not owning the file lock. Backing up the data to %s Note Please make sure pygtk is installed. Plot Plot Weight Plot weight data Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Right: Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Select date range of plot Show Plan Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Track Bodyfat Track Muscle Track Water Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Water Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-30 13:19+0200
Last-Translator: Peter Landgren <peter.talken@telia.com>
Language-Team: Swedish <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1)
X-Generator: Lokalize 1.0
 Lägg till data Lägg till data All tid Ett fel inträffade under import. En annan instans av pondus verkar hålla,på ett redigera samma datafil. Vill du verkligen fortsätta och förlora alla ändringar från den andra instansen? Lägg till notiser till datamängder Body Mass Index Kroppsfett CSV-export CSV-fil att läsa från: CSV-fil sparas till: CSV-import Kunde inte hitta %s Anpassad Vänstra data: Data att exportera: Datafil låst, fortsätta? Datum Datum (YYYY-MM-DD) Tag bort valda data Vill du verkligen ta bort dessa data? Redigera data Redigera valda data Tillåt viktsplan Slutdatum Fel: Ogiltig fil Fel: Felaktigt format Lyckad export Importera data till: Importen misslyckades Import lyckades Senaste 6 månaderna Senaste månad Förra året Matplotlib ej installerat! Muskel Inget Äger ej fillåsningen. Sparar data till %s Notis Se till att pygtk är installerat. Diagram Rita viktkurva Rita viktsdata Inställningar Föredraget enhetssystem: Avsluta Läser fil %s Kom ihåg fönsterstorlek Tag bort data? Högra: Spara diagram Spara som filtyp: Spara till fil Sparar diagram till %s Välj dataområde Välj fil Välj datumutsträckning för diagram Visa plan Utjämnad Startdatum Den inmatade uppgiften har ej rätt format! Exporten lyckades. Den angivna sökvägen pekar inte på en giltig fil! Importen lyckades. Startdatum måste föregå slutdatum! Viktplanen kan göras tillgänglig i inställningsdialogen. För att rita ditt BMI, måste du mata in din längd i inställningsdialogen. Följ kroppsfett Följ muskel Följ vatten Använda kalender i tilläggsdialog Använd en kalender i stället för textinmatning av datum i tilläggs/redigera-dialogen Användarens längd: Använder standardfil ~/.pondus/user_data.xml i stället Vatten Vikt Viktmätningar Viktplan Viktsplan Dette är en mapp, ej en fil! cm fot engelskt tum kg pund m metriskt python matlibplot är ej installerat, plottning ej möjligt! vikt.csv viktdiagram.png 